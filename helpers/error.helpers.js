
/**
 * Helper class for type errors
 */
export class TypeError extends Error {
    /**
     * Throws an error if argument types does not match types names
     * @param {any[]} args 
     * @param {string[]} types
     * @extends Error
     */
    constructor(args, types) {
        this.errors = [];
        for (var i in args) {
            if (typeof args[i] !== types[i])
                this.errors.push(`argument ${i} expected typeof ${types[i]} got ${typeof (args[i])}`);
        }
        super(errors.join("\n\r"));
    }
    /**
     * to check if there is an error
     */
    hasError() {
        return this.errors > 0;
    }
}