/**
 * palceholder for r2p map Adaptor;
 */
var Adaptor = google.maps


/**
 * r2pMap
 * @property {Adaptor} map
 * @property {MapOptions} Options readonly
 */
class r2pMaps extends Eventing {
    /**
     * 
     * @param {MapOptions} options 
     */
    constructor(options) {
        super();
        if (!options.MapType) {
            options.MapType = r2pMapTypes.POI;
        }
        if (typeof options.container === "string") {
            options.container = document.querySelector(container);
        }

        this.Options = options;

    }
    /**
     * initilizes the map
     * @async
     * @returns {Promise<Adaptor>}
     */
    async Initilize() {
        this.map = await this.MapAsync(this.Options.Element, {
            center: this.Options.InitialCenter,
            zoom: this.Options.Zoom
        });
        await this.UpdateData();

        this.map.addListener("idle", this.Idle.bind(this));

        return this.map;
    }

    Idle() {
        this.UpdateData();
    }

    /**
     * Loads map (resolves with tiles are loaded)
     * @param {*} div 
     * @param {*} options 
     */
    MapAsync(div, options) {
        return new Promise((resolve, reject) => {
            try {
                var map = new Adaptor.Map(div, options);
                Adaptor.event.addListenerOnce(map, "tilesloaded", () => {
                    resolve(map);
                })
            }
            catch (ex) {
                reject(ex);
            }
        });
    }

    async UpdateData() {
        var json = await this.GetGeoJson();
        var layer = this.AddJsonLayer(json);
        if (this.Layer)
            this.RemoveJsonLayer(this.Layer);
        this.Layer = layer;
    }

    /**
     * Gets geojson from server, using the parameters set as options in constructor. see: {MapOptions}
     */
    async GetGeoJson() {
        let bounds = this.map.getBounds().toJSON();

        var response = await fetch(this.Options.data.url, {
            method: "POST",
            mode: "cors",
            cache: "no-cache",
            body: JSON.stringify({
                bounds: {
                    ne: { lat: bounds.north, lng: bounds.east },
                    sw: { lat: bounds.south, lng: bounds.west }
                },
                zoom: this.map.getZoom()
            })
        });
        return await response.json();
    }
    /**
     * Adds a new layer on the map
     * @returns {object} layer
     */
    AddJsonLayer(data) {
        var layer = new adaptor.Data();
        layer.AddGeoJson(data, {
            idProperyName: this.Options.data.idProperyName
        });
        layer.addListener("layer", this.LayerClick.bind(this));
    }

    LayerClick(ev) {
        var feature = ev.feature;
        var featureType = int.Parse(feature.getProperty("featureType"));
        if (featureType) {
            switch (featureType) {
                case FeatureTypes.CLUSTOR: this.clustorClick(feature); break;
                case FeatureTypes.POINT_OF_INTREST: this.FireEvent("PoiClick", feature); break;
                case FeatureTypes.MARKER: this.FireEvent("MarkerClick", feature); break;
                case FeatureTypes.CUSTOM: this.FireEvent(feature.getProperty("action", feature)); break;
            }
        }
    }
    /**
     * Removes a layer from the map
     * @param {object} layer 
     */
    RemoveJsonLayer(layer) {
        layer.setMap(null);
    }

    FitBounds(bounds) {
        this.map.fitBounds(bounds, 20);
    }
    GotoCordinate(latLng) {
        this.map.setCenter(latLng);
    }
    GotoMarker(marker) {
        var center = marker.getPosition();
        this.GotoCordinate(center);
    }

    clustorClick(feature) {
        this.FireEvent("ClustorClick", feature);
        feature.toGeoJson(obj=>{
            var b = obj.getProperty("bounds");
            var cordsSw = b.sw;
            var cordsNe = b.ne;
            let bounds = new adaptor.LatLngBounds(new adaptor.LatLng(cordsSw[0], cordsSW[1]), new adaptor.LatLng(cordsNe[0], cordsNe[1]));

            this.FitBounds(bounds);
        });
    }
}
class r2pMapTypes {
    static get POI() {return 0;} //In firefox we can't use static fields so we use static properties insted
    static get Online() {return 1;} //In firefox we can't use static fields so we use static properties insted
}
class FeatureTypes {
    static get CLUSTOR() {return 0;} //In firefox we can't use static fields so we use static properties insted
    static get POINT_OF_INTREST() {return 1;} //In firefox we can't use static fields so we use static properties insted
    static get MARKER() {return 2}; //In firefox we can't use static fields so we use static properties insted
    static get CUSTOM() {return 3}; //In firefox we can't use static fields so we use static properties insted
}

class Eventing {
    constructor() {
        this.events = {};
    }
    AddListener(eventname, callback) {
        this.events[eventname] = callback;
    }
    RemoveListener(eventName) {
        this.events[eventName] = null;
    }
    FireEvent(eventName, ev) {
        if (!this.events[eventName]) {
            return;
        }
        if (ev) {
            this.events[eventName](ev);
            return;
        }
        this.events[eventName]();
    }
}

/**
 * @interface MapOptions
 * @property {r2pMapTypes} MapType defaults to POI
 * @property {Element|string} container
 * @property {Data} data
 * @property {LatLngLiteral} InitialCenter
 * @property {number} Zoom
 */

/**
 * @interface Data
 * @property {string} Url
 * @property {string} idProperyName
 * @property {number} updateInteval how often the data will autoupdate
 */