import * as helpers from "../helpers/error.helpers";
/**
 * Cross browser event handler registration. This listener is removed by calling removeListener(handle) for the handle that is returned by this function.
 * @param {object} instance 
 * @param {string} eventName 
 * @param {Function} handler
 * @returns {MapsEventListener}
 */
export function addDomListener(instance, eventName, handler) {
    google.maps.event.addDomListener(instance, eventName, handler);
}
/**
 * Wrapper around addDomListener that removes the listener after the first event.
 * @param {object} instance 
 * @param {string} eventName 
 * @param {Function} handler 
 * @returns {MapsEventListener}
 * 
 */
export function addListener(instance, eventName, handler) {
    google.maps.event.addListener(instance, eventName, handler);
}
/**
 * Adds the given listener function to the given event name for the given object instance. Returns an identifier for this listener that can be used with removeListener().
 * @param {object} instance 
 * @param {string} eventName 
 * @param {Function} handler 
 * @returns {MapsEventListener}
 * 
 */
export function addDomListenerOnce(instance, eventName, handler) {
    google.maps.event.addDomListenerOnce(instance, eventName, handler);
}
/**
 * Like addListener, but the handler removes itself after handling the first event.
 * @param {object} instance 
 * @param {string} eventName 
 * @param {Function} handler 
 * @returns {MapsEventListener}
 */
export function addListenerOnce(instance, eventName, handler) {
    google.maps.event.addListenerOnce(instance, eventName, handler);
}
/**
 * Removes all listeners for all events for the given instance.
 * @param {object} instance 
 */
export function clearInstanceListeners(instance) {
    google.maps.event.clearInstanceListeners(instance);
}
/**
 * Removes all listeners for the given event for the given instance.
 * @param {object} instance 
 * @param {string} eventName 
 */
export function clearListeners(instance, eventName) {
    google.maps.event.clearListeners(instance, eventName);
}
/**
 * Removes the given listener, which should have been returned by addListener above. Equivalent to calling listener.remove().
 * @param {MapsEventListener} listener 
 */
export function removeListener(listener) {
    google.maps.event.removeListener(listener);
}
/**
 * Triggers the given event. All arguments after eventName are passed as arguments to the listeners.
 * @param {object} instance 
 * @param {string} eventName 
 * @param {*} eventArgs 
 */
export function trigger(instance, eventName, eventArgs) {
    google.maps.event.trigger(instance, eventName, eventArgs);
}

/**
 * This class extends MVCObject
 */
export class Map extends MVCObject {
    /**
     * Creates a new map inside of the given HTML container, which is typically a DIV element.
     * @param {Element} mapDiv 
     * @param {MapOptions} opts 
     */
    constructor(mapDiv, opts) {
        this.gmap = new google.maps.Map(mapDiv, opts);

        /**
         * Additional controls to attach to the map. To add a control to the map, add the control's <div> to the MVCArray corresponding to the ControlPosition where it should be rendered.
         * @property {Array<MVCArray<Node>>} controls
         */
        this.controls = this.gmap.controls;
        /**
         * An instance of Data, bound to the map. Add features to this Data object to conveniently display them on this map.
         * @property {Data} data
         */
        this.data = this.gmap.data;

        /**
         * A registry of MapType instances by string ID.
         * @property {MapTypeRegistry} mapTypes
         */
        this.mapTypes = this.gmap.mapTypes;

        /**
         * @property {MVCArray<MapType>} overlayMapTypes
         */
        this.overlayMapTypes = this.gmap.overlayMapTypes;
    }

    /**
     * Sets the viewport to contain the given bounds.
     * Note: When the map is set to display: none, the fitBounds function reads the map's size as 0x0, and therefore does not do anything. To change the viewport while the map is hidden, set the map to visibility: hidden, thereby ensuring the map div has an actual size.
     * @param {LatLngBounds} bounds 
     * @param {number|padding} [padding] 
     */
    fitBounds(bounds, padding) {
        if (padding)
            this.gmap.fitBounds(bounds.LatLng, padding);
        this.gmap.fitBounds(bounds)
    }
    /**
     * Returns the lat/lng bounds of the current viewport. If more than one copy of the world is visible, the bounds range in longitude from -180 to 180 degrees inclusive. If the map is not yet initialized (i.e. the mapType is still null), or center and zoom have not been set then the result is null or undefined.
     * @returns {LatLngBounds}
     */
    getBounds() {
        var gBounds = this.gmap.getBounds();
        return Converters.BoundsToBounds(gBounds, false);
    }

    /**
     * Returns the position displayed at the center of the map. Note that this LatLng object is not wrapped. See LatLng for more information.
     * @returns {LatLng}
     */
    getCenter() {
        var gLatLng = this.gmap.getCenter();
        return LatLngToLatLng(gLatLng, false);
    }

    /**
     * Returns the clickability of the map icons. A map icon represents a point of interest, also known as a POI. If the returned value is true, then the icons are clickable on the map.
     * @returns {boolean}
     */
    getClickableIcons() {
        return this.gmap.getClickableIcons()
    }
    /**
     * Returns the div that is uses as conmtainer for map ** Or as a developer that is my best geuss **
     * @returns {Element}
     */
    getDiv() {
        return this.gmap.getDiv();
    }
    /**
     * Returns the compass heading of aerial imagery. The heading value is measured in degrees (clockwise) from cardinal direction North.
     * @returns {number}
     */
    getHeading() {
        return this.gmap.getHeading();
    }
    /**
     * 
     * @returns {MapTypeId}
     */
    getMapTypeId() {
        return Converters.MapTypeIdToMapTypeId(this.gmap.getMapTypeId());
    }
    /**
     * Returns the current Projection. If the map is not yet initialized (i.e. the mapType is still null) then the result is null. Listen to projection_changed and check its value to ensure it is not null.
     * @returns {Projection}
     */
    getProjection() {
        return new Projection(this.gmap.getProjection());
    }
    /**
     * @deprecated
     */
    getStreetView() {
        throw new Error("Not yet implemented");
    }
    /**
     * Returns the current angle of incidence of the map, in degrees from the viewport plane to the map plane. The result will be 0 for imagery taken directly overhead or 45 for 45° imagery. 45° imagery is only available for satellite and hybrid map types, within some locations, and at some zoom levels. Note: This method does not return the value set by setTilt. See setTilt for details.
     * @returns {number}
     */
    getTilt() {
        return this.gmap.getTilt();
    }
    /**
     * Returns the Zoom level of the map.
     * @returns {number}
     */
    getZoom() {
        return this.gmap.getZoom();
    }
    /**
     * Changes the center of the map by the given distance in pixels. If the distance is less than both the width and height of the map, the transition will be smoothly animated. Note that the map coordinate system increases from west to east (for x values) and north to south (for y values).
     * @param {number} x 
     * @param {number} y 
     */
    panBy(x, y) {
        this.gmap.panBy(x, y);
    }

    /**
     * Changes the center of the map to the given LatLng. If the change is less than both the width and height of the map, the transition will be smoothly animated.
     * @param {LatLng} latLng 
     */
    panTo(latLng) {
        this.gmap.panTo(latLng.LatLng);
    }

    /**
     * Pans the map by the minimum amount necessary to contain the given LatLngBounds. It makes no guarantee where on the map the bounds will be, except that the map will be panned to show as much of the bounds as possible inside {currentMapSizeInPx} - {padding}.
     * @param {LatLngBounds} bounds 
     * @param {number} [padding] 
     */
    panToBounds(bounds, padding) {
        if (padding) {
            this.gmap.panToBounds(bounds, padding);
            return;
        }
        this.gmap.panToBounds(bounds);
    }
    /**
     * Sets the center
     * @param {LatLng} latLng 
     */
    setCenter(latLng) {
        this.gmap.setCenter(latLng.latLng);
    }
    /**
     * Controls whether the map icons are clickable or not. A map icon represents a point of interest, also known as a POI. To disable the clickability of map icons, pass a value of false to this method
     * @param {boolean} value 
     */
    setClickableIcons(value) {
        this.gmap.setClickableIcons(value);
    }
    /**
     * Sets the compass heading for aerial imagery measured in degrees from cardinal direction North.
     * @param {number} heading 
     */
    setHeading(heading) {
        this.gmap.setHeading(heading);
    }
    /**
     * 
     * @param {MapTypeId} mapType 
     */
    setMapTypeId(mapType) {
        var gmapType = Converters.MapTypeIdToMapTypeId(mapType, true);
        this.gmap.setMapTypeId(gmapType);
    }
    /**
     * 
     * @param {MapOptions} opts 
     */
    setOptions(opts) {
        this.gmap.setOptions(toGoogleMapsOptipons(opts));
    }
    /**
     * @deprecated
     */
    setStreetView() {
        throw new Error("Not implemented");
    }
    /**
     * Controls the automatic switching behavior for the angle of incidence of the map. The only allowed values are 0 and 45. setTilt(0) causes the map to always use a 0° overhead view regardless of the zoom level and viewport. setTilt(45) causes the tilt angle to automatically switch to 45 whenever 45° imagery is available for the current zoom level and viewport, and switch back to 0 whenever 45° imagery is not available (this is the default behavior). 45° imagery is only available for satellite and hybrid map types, within some locations, and at some zoom levels. Note: getTilt returns the current tilt angle, not the value set by setTilt. Because getTilt and setTilt refer to different things, do not bind() the tilt property; doing so may yield unpredictable effects.
     * @param {number} tilt 
     */
    setTilt(tilt) {
        this.gmap.setTilt(tilt);
    }
    /**
     * 
     * @param {number} zoom 
     */
    setZoom(zoom) {
        this.gmap.setZoom(zoom);
    }

}
/**
 * Projection
 */
export class Projection {
    /**
     * 
     * @param {any} gProjection 
     */
    constructor(gProjection) {
        this.gProjection = gProjection;
    }
    /**
     * Translates from the LatLng cylinder to the Point plane. This interface specifies a function which implements translation from given LatLng values to world coordinates on the map projection. The Maps API calls this method when it needs to plot locations on screen. Projection objects must implement this method, but may return null if the projection cannot calculate the Point.
     * @param {LatLng} latLng 
     * @param {Point} [point]
     * @returns {Point} 
     */
    fromLatLngToPoint(latLng, point) {
        var gPoint = this.gProjection.fromLatLngToPoint(latLng.LatLng);
        point = new Point(gPoint.x, point.y);
        return point;
    }
    /**
     * 
     * @param {Point} pixel 
     */
    fromPointToLatLng(pixel) {
        var gLatLng = this.gProjection.fromPointToLatLng(point.point);
        return new LatLng(gLatLng.lat(), gLatLng.lng());
    }
}

/**
 * A registry for MapType instances, keyed by MapType id.
 * This class extends MVCObject.
 */
export class MapTypeRegistry {
    /**
     * The MapTypeRegistry holds the collection of custom map types available to the map for its use. The API consults this registry when providing the list of avaiable map types within controls, for example.
     */
    constructor() {
        this.gMTR = new google.maps.MapTypeRegistry();
    }
    /**
     * Sets the registry to associate the passed string identifier with the passed MapType.
     * @param {string} id 
     * @param {MapType} mapType 
     */
    set(id, mapType) {
        this.gMTR.set(id, mapType.mapType)
    }
}
/**
 * A traffic layer. This class extends MVCObject.
 */
export class TrafficLayer {
    /**
     * A layer that displays current road traffic.
     * @param {TrafficLayerOptions} [opts] 
     */
    constructor(opts) {
        this.trafficLayer = null;
        if (opts) {
            opts.map = opts.map.gmap;
            this.trafficLayer = new google.maps.TrafficLayer(opts);
        }
        this.trafficLayer = new google.map.TrafficLayer();
    }
    /**
     * Renders the layer on the specified map. If map is set to null, the layer will be removed.
     * @param {Map} map 
     */
    setMap(map) {
        this.trafficLayer.setMap(map.gmap);
    }
    /**
     * 
     * @param {TrafficLayerOptions} options 
     */
    setOptions(options) {
        if (options.map)
            options.map = options.map.gmap;

        this.trafficLayer.setOptions(options);
    }
}
/**
 * Base class implementing KVO. 
 */
export class MVCObject {
    /**
     * 
     * @param {string} eventName 
     * @param {Function} handler 
     */
    addListener(eventName, handler) {
        addListener(this, eventName, handler);
    }
}

/**
 * This class extends {MVCObject}.
 */
export class Marker extends MVCObject {
    /**
     * Creates a marker with the options specified. If a map is specified, the marker is added to the map upon construction. Note that the position must be set for the marker to display.
     * @param {MarkerOptions} [opts] 
     */
    constructor(opts) {
        this.Marker = null;
        if(opts) {
            this.Marker = new google.maps.Marker(opts);
        } else {
            this.Marker = new google.maps.Marker();
        }
    }
    /**
     * @returns {Animation}
     */
    getAnimation() {
        return this.Marker.getAnimation();
    }
    /**
     * @returns {boolean}
     */
    getClickable() {
        return this.Marker.getClickable(); 
    }
    getCursor() {
        return this.Marker.getCursor();
    }

}

/**
 * LatLng A LatLng is a point in geographical coordinates: latitude and longitude.
 * * Latitude ranges between -90 and 90 degrees, inclusive. Values above or below this range will be clamped to the range [-90, 90]. This means that if the value specified is less than -90, it will be set to -90. And if the value is greater than 90, it will be set to 90.
 * * Longitude ranges between -180 and 180 degrees, inclusive. Values above or below this range will be wrapped so that they fall within the range. For example, a value of -190 will be converted to 170. A value of 190 will be converted to -170. This reflects the fact that longitudes wrap around the globe.
 * Although the default map projection associates longitude with the x-coordinate of the map, and latitude with the y-coordinate, the latitude coordinate is always written first, followed by the longitude.
 * Notice that you cannot modify the coordinates of a LatLng. If you want to compute another point, you have to create a new one.
 * Most methods that accept LatLng objects also accept a LatLngLiteral object, so that the following are equivalent: 
 * 
 * ````js
 *  map.setCenter(new google.maps.LatLng(-34, 151));
 * ````
 */
export class LatLng {
    /**
     * @property LatLng
     * @private
     */
    LatLng;

    /**
     * @constructor
     * @param {number} lat 
     * @param {number} lng
     * 
     */
    constructor(lat, lng) {
        var err = new helpers.TypeError([lat, lng], ["number", "number"]);
        if (err.hasError())
            throw err;
        this.LatLng = new google.maps.LatLng(lat, lng, noWrap);

        this.lng = this.LatLng.lng;
    }

    /**
     * Returns the latitude in degrees.
     * @returns {number} 
     */
    lat() {
        return this.LatLng.lat;
    }

    /**
     * Returns the longitude in degrees
     * @returns {number}
     */
    lng() {
        return this.LatLng.lng;
    }
    /**
     * Comparison function.
     * @param {LatLng} other
     * @returns {boolean}
     */
    equals(other) {
        var err = new helpers.TypeError([other], ["object"]);
        if (err.hasError())
            throw err;
        return this.LatLng.equals(other.LatLng);
    }
    /**
     * Converts to JSON representation. This function is intended to be used via JSON.stringify
     * @returns {object}
     */
    toJson() {
        return this.LatLng.toJson();
    }
    /**
     * Converts to string representation.
     * @returns {string}
     */
    toString() {
        return this.LatLng.toString();
    }
    /**
     * Returns a string of the form "lat,lng" for this LatLng. We round the lat/lng values to 6 decimal places by default.
     * @param {number} [precision]
     * @returns {string} 
     */
    toUrlLValue(precision) {
        var err = new helpers.TypeError([precision], ["number"]);
        if (err.hasError())
            throw err;
        if (precision)
            return this.LatLng.toUrlLValue(precision);
        return this.LatLng.toUrlLValue();
    }
}


/**
 * LatLngBounds class
 * A LatLngBounds instance represents a rectangle in geographical coordinates, including one that crosses the 180 degrees longitudinal meridian.
 */
export class LatLngBounds {
    /**
    * Constructs a rectangle from the points at its south-west and north-east corners.
    * @param {LatLng} [sw] south-west corner
    * @param {LatLng} [ne] north-east corner 
    */
    constructor(sw, ne) {
        var err = new helpers.TypeError([sw, ne], ["object", "object"]);
        if (err.hasError())
            throw err;

        /**
         * @private
         * @type {LatLngBounds}
         */
        this.llBounds = new google.maps.LatLngBounds(sw.LatLng, ne.LatLng)
    }

    /**
     * Returns true if the given lat/lng is in this bounds.
     * @param {LatLng} latLng
     * @returns {boolean}
     */
    contains(latLng) {
        var err = new helpers.TypeError([latLng], ["object"]);
        if (err.hasError())
            throw err;
        return this.llBounds.contains(latLng.LatLng);
    }
    /**
     * Returns true if this bounds approximately equals the given bounds.
     * @param {LatLng} other
     * @returns {boolean} 
     */
    equals(other) {
        var err = new helpers.TypeError([other], ["object"]);
        if (err.hasError())
            throw err;
        return this.llBounds.equals(other.LatLng);
    }
    /**
     * Extends this bounds to contain the given point.
     * @param {LatLng} point
     * @returns {LatLngBounds} 
     */
    extend(point) {
        var err = new helpers.TypeError([other], ["object"]);
        if (err.hasError())
            throw err;
        this.llBounds = this.llBounds.extend(point.LatLng);
        return this;
    }
    /**
     * Computes the center of this LatLngBounds
     * @returns {LatLng}
     */
    getCenter() {
        var gCenter = this.llBounds.getCenter();
        return new LatLng(gCenter.lat(), gCenter.lng());
    }
    /**
     * Returns the north-east corner of this bounds
     * @returns {LatLng}
     */
    getNorthEast() {
        var gNorthEast = this.llBounds.getNorthEast();
        return new LatLng(gNorthEast.lat(), gNorthEast.lng());
    }
    /**
     * Returns the south-west corner of this bounds.
     * @returns {LatLng}
     */
    getSouthWest() {
        var gSouthWest = this.llBounds.getSouthWest();
        return new LatLng(gSouthWest.lat(), gSouthWest.lng());
    }
    /**
     * Returns true if this bounds shares any points with the other bounds.
     * @param {LatLngBounds} other 
     * @returns {boolean}
     */
    intersects(other) {
        var err = new helpers.TypeError([other], ["object"])
        if (err.hasError())
            throw err;

        return this.llBounds.intersects(other.llBounds);
    }
    /**
     * Returns if the bounds are empty.
     * @returns {boolean}
     */
    isEmpty() {
        return this.llBounds.isEmpty();
    }
    /**
     * Converts to JSON representation. This function is intended to be used via `JSON.stringify`
     * @returns {object}
     */
    toJson() {
        return this.llBounds.toJson();
    }
    /**
     * Converts the given map bounds to a lat/lng span.
     * @returns {LatLng}
     */
    toSpan() {
        var gSpan = this.llBounds.toSpan();
        return new LatLng(gSpan.lat(), gSpan.lng());
    }
    /**
     * Converts to string
     * @returns {string}
     */
    toString() {
        return this.llBounds.toString();
    }
    /**
     * Returns a string of the form "lat_lo,lng_lo,lat_hi,lng_hi" for this bounds, where "lo" corresponds to the southwest corner of the bounding box, while "hi" corresponds to the northeast corner of that box.
     * @param {number} [precision]
     * @returns {string}
     */
    toUrlLValue(precision) {
        var err = new helpers.TypeError([precision], ["number"]);
        if (err.hasError())
            throw err;
        if (!precision)
            return this.llBounds.toUrlLValue();
        return this.llBounds.toUrlLValue(precision);
    }
    /**
     * Extends this bounds to contain the union of this and the given bounds.
     * @param {LatLngBounds} other 
     * @returns {LatLngBounds}
     */
    union(other) {
        var err = new helpers.TypeError([other], ["object"]);
        if (err.hasError())
            throw err;

        var gOther = other.llBounds;
        return this.llBounds.union(gOther);
    }

}

/**
 * Point class
 */
export class Point {
    /**
     * A point on a two-dimensional plane.
     * @param {number} x 
     * @param {number} y 
     */
    constructor(x, y) {
        /**
         * @protected
         * @private
         * @property {google.maps.Point} point
         */
        this.point = new google.maps.Point(x, y);
        /**
         * The X coordinate
         * @property {number} Point.x
         */
        this.x = x;
        /**
         * The Y coordinate
         * @@property {number} Point.y
         */
        this.y = y;
    }
    /**
     * Compares two Points
     * @param {Point} other
     * @returns {boolean}
     */
    equals(other) {
        var err = new helpers.TypeError([other], ["object"]);
        return this.point.equals(other.point);
    }
    /**
     * Returns a string representation of this Point.
     * @returns {string}
     */
    toString() {
        return this.point.toString();
    }
}
/**
 * Size class
 */
export class Size {
    /**
     * Two-dimensional size, where width is the distance on the x-axis, and height is the distance on the y-axis.
     * @param {number} width 
     * @param {number} height 
     * @param {string} [widthUint]
     * @param {string} [heightUnit]
     */
    constructor(width, height, widthUint, heightUnit) {
        this.size = null;
        if (!widthUint) {
            this.size = new google.maps.Size(width, height);
        } else {
            this.size = new google.maps.Size(width, height, widthUint, heightUnit);
        }
        /**
         * @property {number} height
         */
        this.height = height;
        /**
         * @property {number} width
         */
        this.width = width;
    }

    /**
     * Compares two Sizes.
     * @param {Size} other
     * @returns {boolean}
     */
    equals(other) {
        return this.size.equals(other.size);
    }
    /**
     * Returns a string representation of this Size.
     * @returns {string}
     */
    toString() {
        return this.size.toString();
    }
}

/**
 * Maptypes
 */
export class MapTypeId {
    /**
     * @constant
     * @static
     */
    static HYBRID = 0;
    /**
     * @constant
     * @static
     */
    static ROADMAP = 1;
    /**
     * @constant
     * @static
     */
    static SATELLITE = 2;
    /**
     * @constant
     */
    static TERRAIN = 3;
}
/**
 * MapTypeControlStyle
 */
export class MapTypeControlStyle {
    /**
     * Uses the default map type control. When the DEFAULT control is shown, it will vary according to window size and other factors. The DEFAULT control may change in future versions of the API.
     * @constant
     * @static
     * 
     */
    static DEFAULT = "default";
    /**
     * A dropdown menu for the screen realestate conscious.
     * @static
     * @constant
     */
    static DROPDOWN_MENU = "dropdown_menu";
    /**
     * The standard horizontal radio buttons bar.
     * @static
     * @constant
     */
    static HORIZONTAL_BAR = "horizontal_bar";
}

/**
 * Identifiers used to specify the placement of controls on the map. Controls are positioned relative to other controls in the same layout position. Controls that are added first are positioned closer to the edge of the map. 
 * +----------------+ 
 * + TL    TC    TR + 
 * + LT          RT + 
 * +                + 
 * + LC          RC + 
 * +                + 
 * + LB          RB + 
 * + BL    BC    BR + 
 * +----------------+ 
 */
export class ControlPosition {
    /**
     * Elements are positioned in the center of the bottom row.
     * @static
     * @constant
     */
    static BOTTOM_CENTER = "bottom_center";
    /**
     * Elements are positioned in the bottom left and flow towards the middle. Elements are positioned to the right of the Google logo.
     * @static
     * @constant
     */
    static BOTTOM_LEFT = "bottom_left";
    /**
     * Elements are positioned in the bottom right and flow towards the middle. Elements are positioned to the left of the copyrights.
     * @static
     * @constant
     */
    static BOTTOM_RIGHT = "bottom_right";
    /**
     * Elements are positioned on the left, above bottom-left elements, and flow upwards.
     * @static
     * @constant
     */
    static LEFT_BOTTOM = "left_bottom";
    /**
     * Elements are positioned in the center of the left side.
     * @static
     * @constant
     */
    static LEFT_CENTER = "left_center";
    /**
     * Elements are positioned on the left, below top-left elements, and flow downwards.
     * @static
     * @constant
     */
    static LEFT_TOP = "left_top";
    /**
     * Elements are positioned on the right, above bottom-right elements, and flow upwards.
     * @static
     * @constant
     */
    static RIGHT_BOTTOM = "right_bottom";
    /**
     * Elements are positioned in the center of the right side.
     * @static
     * @constant
     */
    static RIGHT_CENTER = "right_center";
    /**
     * Elements are positioned on the right, below top-right elements, and flow downwards.
     * @static
     * @constant
     */
    static RIGHT_TOP = "right_top";
    /**
     * Elements are positioned in the center of the top row.
     * @static
     * @constant
     */
    static TOP_CENTER = "top_center";
    /**
     * Elements are positioned in the top left and flow towards the middle.
     * @static
     * @constant
     */
    static TOP_LEFT = "top_left";
    /**
     * Elements are positioned in the top right and flow towards the middle.
     * @static
     * @constant
     */
    static TOP_RIGHT = "top_right";
}

class Converters {

    static LatLngToLatLng(latLng, reverse) {
        if (reverse) {
            return latLng.LatLng;
        }
        return new LatLng(google.lat(), google.lng());
    }
    static BoundsToBounds(bounds, reverse) {
        if (reverse) {
            return bounds.llBounds;
        }
        var gNorthEast = bounds.getNorthEast();
        var ne = new LatLng(gNorthEast.lat(), gNorthEast.lng());

        var gSouthWest = bounds.getSouthWest();
        var sw = new LatLng(gSouthWest.lat(), gSouthWest.lng());

        return new LatLngBounds(sw, ne);
    }
    static MapTypeIdToMapTypeId(mapType, reverse) {
        if (reverse) {
            switch (mapType) {
                case 0: return google.maps.MapTypeId.HYBRID;
                case 1: return google.maps.MapTypeId.ROADMAP;
                case 2: return google.maps.MapTypeId.SATELLITE;
                case 3: return google.maps.MapTypeId.TERRAIN;
            }
        }

        switch (mapType) {
            case google.maps.MapTypeId.HYBRID: return MapTypeId.HYBRID;
            case google.maps.MapTypeId.ROADMAP: return MapTypeId.ROADMAP;
            case google.maps.MapTypeId.SATELLITE: return MapTypeId.SATELLITE;
            case google.maps.MapTypeId.TERRAIN: return MapTypeId.TERRAIN;
        }
    }

    static toGoogleMapsOptipons(opts) {
        opts.center = opts.center.LatLng;
        opts.mapTypeId = this.MapTypeIdToMapTypeId(opts.mapTypeId, true);

    }

}


/**
 * This event is fired when the viewport bounds have changed.
 * @event Map#bounds_changed
 */
/**
 * This event is fired when the map center property changes.
 * @event Map#center_changed
 */
/**
 * This event is fired when the user clicks on the map. An ApiMouseEvent with properties for the clicked location is returned unless a place icon was clicked, in which case an IconMouseEvent with a placeid is returned. IconMouseEvent and ApiMouseEvent are identical, except that IconMouseEvent has the placeid field. The event can always be treated as an ApiMouseEvent when the placeid is not important. The click event is not fired if a marker or infowindow was clicked.
 * @event Map#click
 * @param {MouseEvent|IconMouseEvent} event
 */
/**
 * This event is fired when the user double-clicks on the map. Note that the click event will also fire, right before this one
 * @event Map#dblclick
 * @param {MouseEvent} event
 */
/**
 * This event is repeatedly fired while the user drags the map.
 * @event Map#drag
 */
/**
 * This event is fired when the user stops dragging the map.
 * @event Map#dragend
 */
/**
 * This event is fired when the user starts dragging the map.
 * @event Map#dragstart
 */
/**
 * This event is fired when the map heading property changes.
 * @event Map#heading_changed
 */
/**
 * This event is fired when the map becomes idle after panning or zooming.
 * @event Map#idle
 */
/**
 * This event is fired when the mapTypeId property changes.
 * @event Map#maptypeid_changed
 */
/**
 * This event is fired whenever the user's mouse moves over the map container.
 * @event Map#mousemove
 * @param {MouseEvent} event
 */
/**
 * This event is fired when the user's mouse exits the map container.
 * @event Map#mouseout
 * @param {MouseEvent} event
 */
/**
 * This event is fired when the user's mouse enters the map container.
 * @event Map#mouseover
 * @param {MouseEvent} event
 */
/**
 * This event is fired when the projection has changed.
 * @event Map#projection_changed
 */
/**
 * This event is fired when the DOM contextmenu event is fired on the map container.
 * @event Map#rightclick
 * @param {MouseEvent} event
 */
/**
 * This event is fired when the visible tiles have finished loading.
 * @event Map#tilesloaded
 */
/**
 * This event is fired when the map tilt property changes.
 * @event Map#tilt_changed
 */
/**
 * This event is fired when the map zoom property changes.
 * @event Map#zoom_changed
 */

/**
 * @interface MapOptions
 * @property {string} backgroundColor Color used for the background of the Map div. This color will be visible when tiles have not yet loaded as the user pans. This option can only be set when the map is initialized.
 * @property {LatLng} center The initial Map center. Required.
 * @property {boolean} clickableIcons When false, map icons are not clickable. A map icon represents a point of interest, also known as a POI. By default map icons are clickable.
 * @property {number} controlSize Size in pixels of the controls appearing on the map. This value must be supplied directly when creating the Map, updating this value later may bring the controls into an undefined state. Only governs the controls made by the Maps API itself. Does not scale developer created custom controls.
 * @property {boolean} disableDefaultUI Enables/disables all default UI. May be overridden individually.
 * @property {boolean} disableDoubleClickZoom Enables/disables zoom and center on double click. Enabled by default. Note: This property is not recommended. To disable zooming on double click, you can use the gestureHandling property, and set it to "none".
 * @property {string} draggableCursor The name or url of the cursor to display when mousing over a draggable map. This property uses the css cursor attribute to change the icon. As with the css property, you must specify at least one fallback cursor that is not a URL. For example: draggableCursor: 'url(http://www.example.com/icon.png), auto;'.
 * @property {string} draggingCursor The name or url of the cursor to display when the map is being dragged. This property uses the css cursor attribute to change the icon. As with the css property, you must specify at least one fallback cursor that is not a URL. For example: draggingCursor: 'url(http://www.example.com/icon.png), auto;'.
 * @property {boolean} fullscreenControl The enabled/disabled state of the Fullscreen control.
 * @property {FullscreenControlOptions} fullscreenControlOptions The display options for the Fullscreen control
 * @property {string} gestureHandling This setting controls how the API handles gestures on the map. Allowed values: "cooperative": Scroll events and one-finger touch gestures scroll the page, and do not zoom or pan the map. Two-finger touch gestures pan and zoom the map. Scroll events with a ctrl key or ⌘ key pressed zoom the map. In this mode the map cooperates with the page. "greedy": All touch gestures and scroll events pan or zoom the map. "none": The map cannot be panned or zoomed by user gestures. "auto": (default) Gesture handling is either cooperative or greedy, depending on whether the page is scrollable or in an iframe.
 * @property {number} heading The heading for aerial imagery in degrees measured clockwise from cardinal direction North. Headings are snapped to the nearest available angle for which imagery is available.
 * @property {boolean} keyboardShortcuts If false, prevents the map from being controlled by the keyboard. Keyboard shortcuts are enabled by default.
 * @property {boolean} mapTypeControl The initial enabled/disabled state of the Map type control.
 * @property {MapTypeControlOptions} mapTypeControlOptions The initial display options for the Map type control.
 * @property {MapTypeId} mapTypeId The initial Map mapTypeId. Defaults to ROADMAP.
 * @property {number} maxZoom The maximum zoom level which will be displayed on the map. If omitted, or set to null, the maximum zoom from the current map type is used instead. Valid values: Integers between zero, and up to the supported maximum zoom level.
 * @property {number} minZoom The minimum zoom level which will be displayed on the map. If omitted, or set to null, the minimum zoom from the current map type is used instead. Valid values: Integers between zero, and up to the supported maximum zoom level.
 * @property {boolean} noClear If true, do not clear the contents of the Map div.
 * @property {boolean} panControl The enabled/disabled state of the Pan control.
 * @property {PanControlOptions} panControlOptions The display options for the Pan control.
 * @property {MapRestriction} restriction Defines a boundary that restricts the area of the map accessible to users. When set, a user can only pan and zoom while the camera view stays inside the limits of the boundary.
 * @property {boolean} rotateControl The enabled/disabled state of the Rotate control.
 * @property {RotateControlOptions} rotateControlOptions The display options for the Rotate control
 * @property {boolean} scaleControl The initial enabled/disabled state of the Scale control
 * @property {ScaleControlOptions} scaleControlOptions The initial display options for the Scale control.
 * @property {boolean} scrollwheel If false, disables zooming on the map using a mouse scroll wheel. The scrollwheel is enabled by default. Note: This property is not recommended. To disable zooming using scrollwheel, you can use the gestureHandling property, and set it to either "cooperative" or "none".
 * @property {Array<MapTypeStyle>} Styles to apply to each of the default map types. Note that for satellite/hybrid and terrain modes, these styles will only apply to labels and geometry.
 * @property {number} tilt Controls the automatic switching behavior for the angle of incidence of the map. The only allowed values are 0 and 45. The value 0 causes the map to always use a 0° overhead view regardless of the zoom level and viewport. The value 45 causes the tilt angle to automatically switch to 45 whenever 45° imagery is available for the current zoom level and viewport, and switch back to 0 whenever 45° imagery is not available (this is the default behavior). 45° imagery is only available for satellite and hybrid map types, within some locations, and at some zoom levels. Note: getTilt returns the current tilt angle, not the value specified by this option. Because getTilt and this option refer to different things, do not bind() the tilt property; doing so may yield unpredictable effects.
 * @property {number} zoom The initial Map zoom level. Required. Valid values: Integers between zero, and up to the supported maximum zoom level.
 * @property {boolean} zoomControl The enabled/disabled state of the Zoom control.
 * @property {ZoomControlOptions} zoomControlOptions The display options for the Zoom control.
 */

/**
 * @interface MapTypeStyle
 * @property {string} elementType The element to which a styler should be applied. An element is a visual aspect of a feature on the map. Example: a label, an icon, the stroke or fill applied to the geometry, and more. Optional. If elementType is not specified, the value is assumed to be 'all'. For details of usage and allowed values, see the style reference.
 * @property {string} featureType The feature, or group of features, to which a styler should be applied. Optional. If featureType is not specified, the value is assumed to be 'all'. For details of usage and allowed values, see the style reference.
 * @property {Array<object>} stylers The style rules to apply to the selected map features and elements. The rules are applied in the order that you specify in this array. For guidelines on usage and allowed values, see the style reference.
 */

/**
 * @interface MapRestriction
 * @property {LatLngBounds} latLngBounds When set, a user can only pan and zoom inside the given bounds. Bounds can restrict both longitude and latitude, or can restrict latitude only. For latitude-only bounds use west and east longitudes of -180 and 180, respectively.
 * @property {boolean} strictBounds By default bounds are relaxed, meaning that a user can zoom out until the entire bounded area is in view. Bounds can be made more restrictive by setting the strictBounds flag to true. This reduces how far a user can zoom out, ensuring that everything outside of the restricted bounds stays hidden.
 */

/**
 * @interface TrafficLayerOptions
 * @property {boolean} autoRefresh Whether the traffic layer refreshes with updated information automatically. This is true by default.
 * @property {Map} map Map on which to display the traffic layer
 */

/**
 * @interface MapsEventListener
 */
/**
 * Calling listener.remove() is equivalent to removeListener(listener).
 * @function
 * @name MapsEventListener#remove
 */

/**
 * Options for the rendering of the fullscreen control.
 * @interface FullscreenControlOptions
 * @property {ControlPosition} position Position id. Used to specify the position of the control on the map. The default position is RIGHT_TOP.
 */

/**
 * @property MapTypeControlOptions
 * @property {Array<MapTypeId>} mapTypeIds IDs of map types to show in the control
 * @property {ControlPosition} position Position id. Used to specify the position of the control on the map. The default position is TOP_RIGHT.
 * @property {MapTypeControlStyle} style Style id. Used to select what style of map type control to display.
 */

/**
 * Options for the rendering of the motion tracking control.
 * @interface MotionTrackingControlOptions
 * @property {ControlPosition} position Position id. Used to specify the position of the control on the map. The default position is RIGHT_TOP.
 */

/**
 * Options for the rendering of the pan control.
 * @interface PanControlOptions
 * @property {ControlPosition} position Position id. Used to specify the position of the control on the map. The default position is TOP_LEFT.
 */

/**
* Options for the rendering of the rotate control.
* @interface RotateControlOptions
* @property {ControlPosition} position Position id. Used to specify the position of the control on the map. The default position is TOP_LEFT.
*/

/**
* Options for the rendering of the rotate control.
* @interface ScaleControlOptions
* @deprecated
* @property {ScaleControlStyle} style Style id. Used to select what style of scale control to display.
*/

/**
* Options for the rendering of the zoom control.
* @interface ZoomControlOptions
* @property {ControlPosition} position Position id. Used to specify the position of the control on the map. The default position is TOP_LEFT.
*/